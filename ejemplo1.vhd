----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:43:57 05/15/2022 
-- Design Name: 
-- Module Name:    ejemplo1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ejemplo1 is
    Port ( A : in  STD_LOGIC_VECTOR(3 downto 0);
           B,C : in  STD_LOGIC;
           Z : out  STD_LOGIC);
			  x: inout 
end ejemplo1;

architecture Behavioral of ejemplo1 is
begin
process (A,B,C) 
variable sBus:  STD_LOGIC_VECTOR(1 downto 0);
variable sin : integer range 0 to 3;
begin 
sBus := B &C;
sin := Conv_integer(sBus);
case sBus is 
when "00" => Z <= A(0);
when "01"=> Z <=A(1);
when "10"=> Z<= A(2);
when others => Z <=A(3);
end case;
end process;
end Behavioral;


begin 
z <= a/b + 
