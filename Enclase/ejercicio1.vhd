
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity EjercicioClase is
Port ( A : in STD_LOGIC;
B : in STD_LOGIC;
C : in STD_LOGIC;
D : in STD_LOGIC;
F1 : out STD_LOGIC);
end EjercicioClase;

architecture Behavioral of EjercicioClase is

begin
f1 <= (A or B) XNOR (C AND D);

end Behavioral;