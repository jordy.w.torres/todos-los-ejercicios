/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/.Xilinx/ensayo1/Proyecto.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_2545490612_503743352(char *, unsigned char , unsigned char );


static void work_a_0491540795_3212880686_p_0(char *t0)
{
    unsigned char t1;
    unsigned char t2;
    unsigned char t3;
    char *t4;
    char *t5;
    int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned char t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned char t26;
    char *t27;
    char *t28;
    int t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    unsigned char t33;
    unsigned char t34;
    unsigned char t35;
    char *t36;
    char *t37;
    int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    unsigned char t42;
    char *t43;
    char *t44;
    int t45;
    unsigned int t46;
    unsigned int t47;
    unsigned int t48;
    unsigned char t49;
    unsigned char t50;
    unsigned char t51;
    char *t52;
    char *t53;
    int t54;
    unsigned int t55;
    unsigned int t56;
    unsigned int t57;
    unsigned char t58;
    char *t59;
    char *t60;
    int t61;
    unsigned int t62;
    unsigned int t63;
    unsigned int t64;
    unsigned char t65;
    unsigned char t66;
    unsigned char t67;
    char *t68;
    char *t69;
    char *t70;
    char *t71;
    char *t72;

LAB0:    xsi_set_current_line(41, ng0);
    t4 = (t0 + 1032U);
    t5 = *((char **)t4);
    t6 = (0 - 0);
    t7 = (t6 * 1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t4 = (t5 + t9);
    t10 = *((unsigned char *)t4);
    t11 = (t0 + 1192U);
    t12 = *((char **)t11);
    t13 = (0 - 0);
    t14 = (t13 * 1);
    t15 = (1U * t14);
    t16 = (0 + t15);
    t11 = (t12 + t16);
    t17 = *((unsigned char *)t11);
    t18 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t10, t17);
    t19 = (t18 == (unsigned char)2);
    if (t19 == 1)
        goto LAB11;

LAB12:    t20 = (t0 + 1032U);
    t21 = *((char **)t20);
    t22 = (1 - 0);
    t23 = (t22 * 1);
    t24 = (1U * t23);
    t25 = (0 + t24);
    t20 = (t21 + t25);
    t26 = *((unsigned char *)t20);
    t27 = (t0 + 1192U);
    t28 = *((char **)t27);
    t29 = (1 - 0);
    t30 = (t29 * 1);
    t31 = (1U * t30);
    t32 = (0 + t31);
    t27 = (t28 + t32);
    t33 = *((unsigned char *)t27);
    t34 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t26, t33);
    t35 = (t34 == (unsigned char)2);
    t3 = t35;

LAB13:    if (t3 == 1)
        goto LAB8;

LAB9:    t36 = (t0 + 1032U);
    t37 = *((char **)t36);
    t38 = (2 - 0);
    t39 = (t38 * 1);
    t40 = (1U * t39);
    t41 = (0 + t40);
    t36 = (t37 + t41);
    t42 = *((unsigned char *)t36);
    t43 = (t0 + 1192U);
    t44 = *((char **)t43);
    t45 = (2 - 0);
    t46 = (t45 * 1);
    t47 = (1U * t46);
    t48 = (0 + t47);
    t43 = (t44 + t48);
    t49 = *((unsigned char *)t43);
    t50 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t42, t49);
    t51 = (t50 == (unsigned char)2);
    t2 = t51;

LAB10:    if (t2 == 1)
        goto LAB5;

LAB6:    t52 = (t0 + 1032U);
    t53 = *((char **)t52);
    t54 = (3 - 0);
    t55 = (t54 * 1);
    t56 = (1U * t55);
    t57 = (0 + t56);
    t52 = (t53 + t57);
    t58 = *((unsigned char *)t52);
    t59 = (t0 + 1192U);
    t60 = *((char **)t59);
    t61 = (3 - 0);
    t62 = (t61 * 1);
    t63 = (1U * t62);
    t64 = (0 + t63);
    t59 = (t60 + t64);
    t65 = *((unsigned char *)t59);
    t66 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t58, t65);
    t67 = (t66 == (unsigned char)2);
    t1 = t67;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(43, ng0);
    t4 = (t0 + 2912);
    t5 = (t4 + 56U);
    t11 = *((char **)t5);
    t12 = (t11 + 56U);
    t20 = *((char **)t12);
    *((unsigned char *)t20) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t4);

LAB3:    t4 = (t0 + 2832);
    *((int *)t4) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(42, ng0);
    t68 = (t0 + 2912);
    t69 = (t68 + 56U);
    t70 = *((char **)t69);
    t71 = (t70 + 56U);
    t72 = *((char **)t71);
    *((unsigned char *)t72) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t68);
    goto LAB3;

LAB5:    t1 = (unsigned char)1;
    goto LAB7;

LAB8:    t2 = (unsigned char)1;
    goto LAB10;

LAB11:    t3 = (unsigned char)1;
    goto LAB13;

}


extern void work_a_0491540795_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0491540795_3212880686_p_0};
	xsi_register_didat("work_a_0491540795_3212880686", "isim/entidad_isim_beh.exe.sim/work/a_0491540795_3212880686.didat");
	xsi_register_executes(pe);
}
